from flask_restful import Resource, abort, reqparse
from flask import request
import sqlite3
from datetime import datetime
import json
from helper import result_to_json, RecipieModel, RecipieResponseModel
from flask_httpauth import HTTPBasicAuth
from flask_restful_swagger_2 import swagger, Schema


db = sqlite3.connect("db.sqlite")


def abort_unknown_id(recipie_id):
    cur = db.cursor()
    cur.execute("SELECT * from recipies where id=?", [recipie_id])
    if cur.fetchone() is None:
        abort(404, message="Recipie {} doesn't exist".format(recipie_id))


parser = reqparse.RequestParser()
parser.add_argument('user_id', type=int, required=True)
parser.add_argument('title', type=str, required=True)
parser.add_argument('text', type=str, required=True)


auth = HTTPBasicAuth()


@auth.get_password
def get_pw(username):
    # cur = db.cursor()
    # cur.execute("SELECT * FROM users where username=?", [username])
    # res = cur.fetchone()
    # if res != None:
    #     return res[2]
    if username=="admin":
        return "admin"
    return None


class Recipie(Resource):
    @auth.login_required
    @swagger.doc({
        'description': 'Get a specific recipie',
        'responses': {'201': {
            'description': 'JSON representation of the requested recipie',
            'examples': {'application/json': {
                "text": "adfhgadfhg",
                "created": "2017-11-27 09:39:05.739000",
                "user_id": 1,
                "id": 1,
                "title": "adfg"
            }},
            'schema': RecipieResponseModel
        },
            '401': {
                'description': 'Unauthorized access'
            }},
        'parameters': [
            {
                'name': 'recipie_id',
                'description': 'Recipie identifier',
                'in': 'path',
                'type': 'integer'
            }
        ]
    })
    def get(self, recipie_id):
        abort_unknown_id(recipie_id)
        cur = db.cursor()
        a = cur.execute("SELECT * FROM recipies where id=?", [recipie_id])
        return result_to_json(a.fetchall(), a.description)

    @auth.login_required
    @swagger.doc({
        'description': 'Delete a recipie',
        'responses': {'204': {
            'description': 'Recipie successfully deleted'
        },
            '401': {
                'description': 'Unauthorized access'
            }},
        'parameters': [
            {
                'name': 'recipie_id',
                'description': 'Recipie identifier',
                'in': 'path',
                'type': 'integer'
            }
        ]
    })
    def delete(self, recipie_id):
        abort_unknown_id(recipie_id)
        cur = db.cursor()
        a = cur.execute("DELETE FROM recipies where id=?", [recipie_id])
        db.commit()
        return "", 204

    @auth.login_required
    @swagger.doc({
        'description': 'Put a new recipie or update an existing recipie',
        'responses': {'201': {
            'description': 'JSON representation of the newly added recipie',
            'examples': {'application/json': {
                "text": "adfhgadfhg",
                "created": "2017-11-27 09:39:05.739000",
                "user_id": 1,
                "id": 1,
                "title": "adfg"
            }},
            'schema': RecipieResponseModel
        },
            '401': {
                'description': 'Unauthorized access'
            }
        },
        'parameters': [
            {
                "name": "recipie_id",
                "type": "integer",
                "in": "path",
                "description": "Recipie identifier"
            },
            {
                "schema": RecipieModel,
                "in": "body",
                "required": "true",
                "name": "body",
                "description": "Request body"
            }
        ]
    })
    def put(self, recipie_id):
        args = parser.parse_args()
        cur = db.cursor()
        user_id = args.get("user_id")
        text = args.get("text")
        title = args.get("title")
        created = datetime.now().isoformat(' ')
        cur.execute("select * from recipies where id=?",[recipie_id])
        if not cur.fetchall():
            a=cur.execute("INSERT INTO recipies (id,user_id,text,title,created) VALUES (?,?,?,?,?)",
                    (recipie_id,user_id, text, title, created))
        else:
            a = cur.execute("UPDATE recipies set user_id=?,text=?,title=?,created=? where id=?",
                        (user_id, text, title, created,recipie_id))
        db.commit()
        return result_to_json(a.fetchall(), a.description), 201


class RecipieList(Resource):
    @auth.login_required
    @swagger.doc({
        'description': 'Get all recipies from the database',
        'responses': {'200': {
            'description': 'JSON list of all recipies',
            'examples': {'application/json': [
                {
                    "text": "adfhgadfhg",
                    "created": "2017-11-27 09:39:05.739000",
                    "user_id": 1,
                    "id": 1,
                    "title": "adfg"
                },
                {
                    "text": "adfghsadfgh",
                    "created": "2017-11-27 09:39:17.500000",
                    "user_id": 1,
                    "id": 2,
                    "title": "dsfghadfh"
                }]},
            'schema': RecipieResponseModel
        },
            '401': {
                'description': 'Unauthorized access'
            }}
    })
    def get(self):
        cur = db.cursor()
        a = cur.execute("SELECT * FROM recipies")
        return result_to_json(a.fetchall(), a.description)

    @auth.login_required
    @swagger.doc({
        'description': 'Post a new recipie',
        'responses': {'201': {
            'description': 'JSON representation of the posted recipie',
            'examples': {'application/json': {
                "text": "adfhgadfhg",
                "created": "2017-11-27 09:39:05.739000",
                "user_id": 1,
                "id": 1,
                "title": "adfg"
            }},
            'schema': RecipieResponseModel
        },
            '401': {
                'description': 'Unauthorized access'
            }},
        'parameters': [
            {
                "schema": RecipieModel,
                "in": "body",
                "required": "true",
                "name": "body",
                "description": "Request body"
            }
        ]
    })
    def post(self):
        args = parser.parse_args()
        user_id = args.get("user_id")
        text = args.get("text")
        title = args.get("title")
        created = datetime.now().isoformat(' ')
        cur = db.cursor()
        cur.execute("INSERT INTO recipies (user_id,text,title,created) VALUES (?,?,?,?)",
                    (user_id, text, title, created))
        a = cur.execute("SELECT * from recipies where id=?", [cur.lastrowid])
        db.commit()
        return result_to_json(a.fetchall(), a.description), 201
