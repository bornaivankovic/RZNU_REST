from flask_restful_swagger_2 import Schema

def result_to_json(rows,descriptions):
    if not rows:
        return ""
    descriptions=[x[0] for x in descriptions]
    res=[]
    for i in rows:
        tmp={}
        for j in range(len(i)):
            tmp[descriptions[j]]=i[j]
        res.append(tmp)
    return res if len(res)>1 else res[0]

class RecipieModel(Schema):
    type="object"
    properties={
        "user_id": {
          "required": "true",
          "type": "integer",
          "name": "user_id",
          "description": "Id of the user posting a recipie"
        },
        "text": {
          "required": "true",
          "type": "string",
          "name": "text",
          "description": "Body of the recipie"
        },
        "title": {
          "required": "true",
          "type": "string",
          "name": "title",
          "description": "Recipie title"
        }
      }

class UserModel(Schema):
    type="object"
    properties={
        "username": {
          "required": "true",
          "type": "string",
          "name": "username",
          "description": "Username of a user"
        },
        "password": {
          "required": "true",
          "type": "string",
          "name": "password",
          "description": "Password of a user"
        },
        "dob": {
          "required": "true",
          "type": "string",
          "name": "dob",
          "description": "Date of birth of a user"
        },
        "bio": {
          "required": "true",
          "type": "string",
          "name": "bio",
          "description": "User biography"
        }
      }

class RecipieResponseModel(Schema):
    type="object"
    properties={
        "id":{
            "type":"integer",
            "name":"id",
            "decription":"Recipie identifier"
        },
        "user_id":{
            "type":"integer",
            "name":"user_id",
            "decription":"Id of the user posting a recipie"
        },
        "title":{
            "type":"string",
            "name":"title",
            "decription":"Title of the recipie"
        },
        "text":{
            "type":"string",
            "name":"text",
            "decription":"Body of the recipie"
        },
        "created":{
            "type":"string",
            "name":"created",
            "decription":"ISO8601 string representation of date and time when the recipie was posted"
        }
    }

class UserResponseModel(Schema):
    type="object"
    properties={
        "id":{
            "type":"integer",
            "name":"id",
            "decription":"User identifier"
        },
        "username": {
          "required": "true",
          "type": "string",
          "name": "username",
          "description": "Username of a user"
        },
        "password": {
          "required": "true",
          "type": "string",
          "name": "password",
          "description": "Password of a user"
        },
        "dob": {
          "required": "true",
          "type": "string",
          "name": "dob",
          "description": "Date of birth of a user"
        },
        "bio": {
          "required": "true",
          "type": "string",
          "name": "bio",
          "description": "User biography"
        }
    }
