from flask_restful import Resource, abort, reqparse
from flask import request
import sqlite3
from datetime import datetime
from helper import result_to_json, RecipieModel, RecipieResponseModel, UserModel, UserResponseModel
from flask_httpauth import HTTPBasicAuth
from flask_restful_swagger_2 import swagger

db = sqlite3.connect("db.sqlite")


def abort_unknown_id(user_id, recipie_id=None):
    cur = db.cursor()
    cur.execute("SELECT * from users where id=?", [user_id])
    if cur.fetchone() is None and recipie_id == None:
        abort(404, message="User {} doesn't exist".format(user_id))
    elif recipie_id is not None:
        cur.execute("SELECT * from recipies where id=? and user_id=?",
                    [recipie_id, user_id])
        if cur.fetchone() is None:
            abort(404, message="User {} and recipie {} combo isn't valid".format(
                user_id, recipie_id))


userParser = reqparse.RequestParser()
userParser.add_argument('username', type=str, required=True)
userParser.add_argument('password', type=str, required=True)
userParser.add_argument('dob', type=str, required=True)
userParser.add_argument('bio', type=str, required=True)

recipieParser = reqparse.RequestParser()
recipieParser.add_argument('title', type=str, required=True)
recipieParser.add_argument('text', type=str, required=True)
auth = HTTPBasicAuth()


@auth.get_password
def get_pw(username):
    # cur = db.cursor()
    # cur.execute("SELECT * FROM users where username=?", [username])
    # res = cur.fetchone()
    # if res != None:
    #     return res[2]
    if username == "admin":
        return "admin"
    return None


class User(Resource):
    @auth.login_required
    @swagger.doc({
        'description': 'Get a specific user',
        'responses': {
            '201': {
                'description': 'JSON representation of the requested user',
                'examples': {'application/json': {
                    "username": "adfbh",
                    "dob": "1991-10-27 09:39:05",
                    "password": "adfg",
                    "id": 1,
                    "bio": "gdfasbh"}},
                'schema': UserResponseModel},
            '401': {
                'description': 'Unauthorized access'
            }
        },
        'parameters': [
            {
                'name': 'user_id',
                'description': 'User identifier',
                'in': 'path',
                'type': 'integer'
            }
        ]
    })
    def get(self, user_id):
        abort_unknown_id(user_id)
        cur = db.cursor()
        a = cur.execute("SELECT * FROM users where id=?", [user_id])
        return result_to_json(a.fetchall(), a.description)

    @auth.login_required
    @swagger.doc({
        'description': 'Delete a user',
        'responses': {'204': {
            'description': 'User successfully deleted'
        },
            '401': {
                'description': 'Unauthorized access'
        }},
        'parameters': [
            {
                'name': 'user_id',
                'description': 'User identifier',
                'in': 'path',
                'type': 'integer'
            }
        ]
    })
    def delete(self, user_id):
        abort_unknown_id(user_id)
        cur = db.cursor()
        a = cur.execute("DELETE FROM users where id=?", [user_id])
        db.commit()
        return "", 204

    @auth.login_required
    @swagger.doc({
        'description': 'Put a new user or update an existing user',
        'responses': {'201': {
            'description': 'JSON representation of the newly added user',
            'examples': {'application/json': {
                "username": "adfbh",
                "dob": "1991-10-27 09:39:05",
                "password": "adfg",
                "id": 1,
                "bio": "gdfasbh"
            }},
            'schema': UserResponseModel
        },
            '401': {
                'description': 'Unauthorized access'
            }
        },
        'parameters': [
            {
                "schema": UserModel,
                "in": "body",
                "required": "true",
                "name": "body",
                "description": "Request body"
            },
            {
                "name": "user_id",
                "type": "integer",
                "in": "path",
                "description": "User identifier"
            }
        ]
    })
    def put(self, user_id):
        args = userParser.parse_args()
        cur = db.cursor()
        username = args.get("username")
        password = args.get("password")
        dob = args.get("dob")
        bio = args.get("bio")
        cur.execute("select * from users where id=?", [user_id])
        if not cur.fetchall():
            a = cur.execute("INSERT INTO users (id,username,password,dob,bio) VALUES (?,?,?,?,?)",
                            (user_id, username, password, dob, bio))
        else:
            a = cur.execute("UPDATE users set username=?,password=?,dob=?,bio=? where id=?",
                            (username, password, dob, bio, user_id))
        db.commit()
        return result_to_json(a.fetchall(), a.description), 201


class UserList(Resource):
    @auth.login_required
    @swagger.doc({
        'description': 'Get all users from the database',
        'responses': {'200': {
            'description': 'JSON list of all users',
            'examples': {'application/json': [
                {
                    "username": "adfbh",
                    "dob": "1991-10-27 09:39:05",
                    "password": "adfg",
                    "id": 1,
                    "bio": "gdfasbh"
                },
                {
                    "username": "adfgadafg",
                    "dob": "1994-10-7 09:39:05",
                    "password": "adfdfgshsdfghg",
                    "id": 2,
                    "bio": "gdfdsfghdsfhgasbh"
                }]},
            'schema': UserResponseModel
        },
            '401': {
                'description': 'Unauthorized access'
            }}
    })
    def get(self):
        cur = db.cursor()
        a = cur.execute("SELECT * FROM users")
        return result_to_json(a.fetchall(), a.description)

    @auth.login_required
    @swagger.doc({
        'description': 'Post a new user',
        'responses': {'201': {
            'description': 'JSON representation of the posted user',
            'examples': {'application/json': {
                "username": "adfbh",
                "dob": "1991-10-27 09:39:05",
                "password": "adfg",
                "id": 1,
                "bio": "gdfasbh"
            }},
            'schema': UserResponseModel
        },
            '401': {
                'description': 'Unauthorized access'
            }},
        'parameters': [
            {
                "schema": UserModel,
                "in": "body",
                "required": "true",
                "name": "body",
                "description": "Request body"
            }
        ]
    })
    def post(self):
        args = userParser.parse_args()
        username = args.get("username")
        password = args.get("password")
        dob = args.get("dob")
        bio = args.get("bio")
        cur = db.cursor()
        cur.execute("INSERT INTO users (username,password,dob,bio) VALUES (?,?,?,?)",
                    (username, password, dob, bio))
        a = cur.execute("SELECT * from users where id=?", [cur.lastrowid])
        db.commit()
        return result_to_json(a.fetchall(), a.description), 201


class UserRecipies(Resource):
    @auth.login_required
    @swagger.doc({
        'description': 'Get a specific recipie of a specific user',
        'responses': {'201': {
            'description': 'JSON representation of the requested recipie',
            'examples': {'application/json': {
                "text": "adfhgadfhg",
                "created": "2017-11-27 09:39:05.739000",
                "user_id": 1,
                "id": 1,
                "title": "adfg"
            }},
            'schema': RecipieResponseModel
        },
            '401': {
                'description': 'Unauthorized access'
            }},
        'parameters': [
            {
                'name': 'user_id',
                'description': 'User identifier',
                'in': 'path',
                'type': 'integer'
            },
            {
                'name': 'recipie_id',
                'description': 'Recipie identifier',
                'in': 'path',
                'type': 'integer'
            }
        ]
    })
    def get(self, user_id, recipie_id):
        abort_unknown_id(user_id, recipie_id)
        cur = db.cursor()
        a = cur.execute(
            "SELECT * FROM recipies where id=? and user_id=?", [recipie_id, user_id])
        return result_to_json(a.fetchall(), a.description)

    @auth.login_required
    @swagger.doc({
        'description': 'Delete a recipie',
        'responses': {'204': {
            'description': 'Recipie successfully deleted'
        },
            '401': {
                'description': 'Unauthorized access'
            }},
        'parameters': [
            {
                'name': 'user_id',
                'description': 'User identifier',
                'in': 'path',
                'type': 'integer'
            },
            {
                'name': 'recipie_id',
                'description': 'Recipie identifier',
                'in': 'path',
                'type': 'integer'
            }
        ]
    })
    def delete(self, user_id, recipie_id):
        abort_unknown_id(user_id, recipie_id)
        cur = db.cursor()
        a = cur.execute("DELETE FROM recipies where id=? and user_id=?", [
                        recipie_id, user_id])
        db.commit()
        return "", 204

    @auth.login_required
    @swagger.doc({
        'description': 'Put a new recipie or update an existing recipie for a specific user',
        'responses': {'201': {
            'description': 'JSON representation of the newly added recipie',
            'examples': {'application/json': {
                "text": "adfhgadfhg",
                "created": "2017-11-27 09:39:05.739000",
                "user_id": 1,
                "id": 1,
                "title": "adfg"
            }},
            'schema': RecipieResponseModel
        },
            '401': {
                'description': 'Unauthorized access'
            }
        },
        'parameters': [
            {
                'name': 'user_id',
                'description': 'User identifier',
                'in': 'path',
                'type': 'integer'
            },
            {
                "name": "recipie_id",
                "type": "integer",
                "in": "path",
                "description": "Recipie identifier"
            },
            {
                "schema": RecipieModel,
                "in": "body",
                "required": "true",
                "name": "body",
                "description": "Request body"
            }
        ]
    })
    def put(self, user_id, recipie_id):
        args = recipieParser.parse_args()
        cur = db.cursor()
        text = args.get("text")
        title = args.get("title")
        created = datetime.now().isoformat(' ')
        cur.execute("select * from recipies where id=?", [recipie_id])
        if not cur.fetchall():
            a = cur.execute("INSERT INTO recipies (id,user_id,text,title,created) VALUES (?,?,?,?,?)",
                            (recipie_id, user_id, text, title, created))
        else:
            a = cur.execute("UPDATE recipies set user_id=?,text=?,title=?,created=? where id=?",
                            (user_id, text, title, created, recipie_id))
        db.commit()
        return result_to_json(a.fetchall(), a.description), 201


class UserRecipiesList(Resource):
    @auth.login_required
    @swagger.doc({
        'description': 'Get all recipies of a specific user from the database',
        'responses': {'200': {
            'description': 'JSON list of all recipies',
            'examples': {'application/json': [
                {
                    "text": "adfhgadfhg",
                    "created": "2017-11-27 09:39:05.739000",
                    "user_id": 1,
                    "id": 1,
                    "title": "adfg"
                },
                {
                    "text": "adfghsadfgh",
                    "created": "2017-11-27 09:39:17.500000",
                    "user_id": 1,
                    "id": 2,
                    "title": "dsfghadfh"
                }]},
            'schema': RecipieResponseModel
        },
            '401': {
                'description': 'Unauthorized access'
            }},
        'parameters': [
            {
                'name': 'user_id',
                'description': 'User identifier',
                'in': 'path',
                'type': 'integer'
            }]
    })
    def get(self, user_id):
        cur = db.cursor()
        a = cur.execute("SELECT * FROM recipies where user_id=?", [user_id])
        return result_to_json(a.fetchall(), a.description)

    @auth.login_required
    @swagger.doc({
        'description': 'Post a new recipie',
        'responses': {'201': {
            'description': 'JSON representation of the posted recipie',
            'examples': {'application/json': {
                "text": "adfhgadfhg",
                "created": "2017-11-27 09:39:05.739000",
                "user_id": 1,
                "id": 1,
                "title": "adfg"
            }},
            'schema': RecipieResponseModel
        },
            '401': {
                'description': 'Unauthorized access'
            }},
        'parameters': [
            {
                'name': 'user_id',
                'description': 'User identifier',
                'in': 'path',
                'type': 'integer'
            },
            {
                "schema": RecipieModel,
                "in": "body",
                "required": "true",
                "name": "body",
                "description": "Request body"
            }
        ]
    })
    def post(self, user_id):
        args = recipieParser.parse_args()
        text = args.get("text")
        title = args.get("title")
        created = datetime.now().isoformat(' ')
        cur = db.cursor()
        cur.execute("INSERT INTO recipies (user_id,text,title,created) VALUES (?,?,?,?)",
                    (user_id, text, title, created))
        a = cur.execute("SELECT * from recipies where id=?", [cur.lastrowid])
        db.commit()
        return result_to_json(a.fetchall(), a.description), 201
