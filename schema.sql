drop table if exists users;
create table users (
  id integer primary key autoincrement,
  username text not null,
  password text not null,
  dob text,
  bio text
);
drop table if exists recipies;
create table recipies(
  id integer primary key autoincrement,
  user_id integer not null,
  'text' text not null,
  title text not null,
  created text not null,
  FOREIGN KEY(user_id) REFERENCES users(id)
);