import requests
import unittest

class TestApi200(unittest.TestCase):
    auth = ("admin", "admin")
    test_user_id=5
    test_recipie_id=5
    """
    /users
    """
    def test_a_get_user(self):
        user_id = 1
        response = requests.get(
            'http://localhost:5000/api/users/' + str(user_id), auth=self.auth)
        self.assertEqual(response.status_code, 200)

    def test_b_get_users(self):
        response = requests.get(
            'http://localhost:5000/api/users', auth=self.auth)
        self.assertEqual(response.status_code, 200)

    def test_c_post_users(self):
        data = {"username": "Test_user",
                "dob": "1991-05-12 09:39:17",
                "password": "password",
                "bio": "Test_user biography"}
        response = requests.post(
            'http://localhost:5000/api/users', data=data, auth=self.auth)
        self.assertEqual(response.status_code, 201)

    def test_d_put_users(self):
        data = {"username": "Test_user",
                "dob": "1991-05-12 09:39:17",
                "password": "password",
                "bio": "Test_user biography"}
        user_id = self.test_user_id
        response = requests.put(
            'http://localhost:5000/api/users/' + str(user_id), data=data, auth=self.auth)
        self.assertEqual(response.status_code, 201)

    def test_e_delete_user(self):
        user_id = self.test_user_id
        response = requests.delete(
            'http://localhost:5000/api/users/' + str(user_id), auth=self.auth)
        self.assertEqual(response.status_code, 204)

    """
    /recipies
    """

    def test_f_get_recipie(self):
        recipie_id = 1
        response = requests.get(
            'http://localhost:5000/api/recipies/' + str(recipie_id), auth=self.auth)
        self.assertEqual(response.status_code, 200)

    def test_g_get_recipies(self):
        response = requests.get(
            'http://localhost:5000/api/recipies', auth=self.auth)
        self.assertEqual(response.status_code, 200)

    def test_h_post_recipies(self):
        data = {"text": "Test recipie text",
                "user_id": 1,
                "title": "Test recipie title"}
        response = requests.post(
            'http://localhost:5000/api/recipies', data=data, auth=self.auth)
        self.assertEqual(response.status_code, 201)

    def test_i_put_recipies(self):
        data = {"text": "Test recipie text",
                "user_id": 1,
                "title": "Test recipie title"}
        recipie_id = self.test_recipie_id
        response = requests.put(
            'http://localhost:5000/api/recipies/' + str(recipie_id), data=data, auth=self.auth)
        self.assertEqual(response.status_code, 201)

    def test_j_delete_recipie(self):
        recipie_id = self.test_recipie_id
        response = requests.delete(
            'http://localhost:5000/api/recipies/' + str(recipie_id), auth=self.auth)
        self.assertEqual(response.status_code, 204)

    """
    /users/{user_id}/recipies
    """
    def test_k_get_user_recipie(self):
        recipie_id = 1
        user_id=1
        response = requests.get(
            'http://localhost:5000/api/users/' + str(user_id)+'/recipies/'+str(recipie_id), auth=self.auth)
        self.assertEqual(response.status_code, 200)

    def test_l_get_user_recipies(self):
        user_id=1
        response = requests.get(
            'http://localhost:5000/api/users/' + str(user_id)+'/recipies', auth=self.auth)
        self.assertEqual(response.status_code, 200)

    def test_m_post_user_recipies(self):
        user_id=1
        data = {"text": "Test recipie text",
                "user_id": user_id,
                "title": "Test recipie title"}
        response = requests.post(
            'http://localhost:5000/api/users/' + str(user_id)+'/recipies', data=data, auth=self.auth)
        self.assertEqual(response.status_code, 201)

    def test_n_put_user_recipies(self):
        user_id=1
        data = {"text": "Test recipie text",
                "user_id":user_id,
                "title": "Test recipie title"}
        recipie_id = self.test_recipie_id
        response = requests.put(
            'http://localhost:5000/api/users/' + str(user_id)+'/recipies/'+str(recipie_id), data=data, auth=self.auth)
        self.assertEqual(response.status_code, 201)

    def test_o_delete_user_recipie(self):
        user_id=1
        recipie_id = self.test_recipie_id
        response = requests.delete(
            'http://localhost:5000/api/users/' + str(user_id)+'/recipies/'+str(recipie_id), auth=self.auth)
        self.assertEqual(response.status_code, 204)

class TestApi401(unittest.TestCase):
    auth = ("badmin", "badmin")

    """
    /users
    """
    def test_get_user(self):
        user_id = 1
        response = requests.get(
            'http://localhost:5000/api/users/' + str(user_id), auth=self.auth)
        self.assertEqual(response.status_code, 401)

    def test_get_users(self):
        response = requests.get(
            'http://localhost:5000/api/users', auth=self.auth)
        self.assertEqual(response.status_code, 401)

    def test_post_users(self):
        data = {"username": "Test_user",
                "dob": "1991-05-12 09:39:17",
                "password": "password",
                "bio": "Test_user biography"}
        response = requests.post(
            'http://localhost:5000/api/users', data=data, auth=self.auth)
        self.assertEqual(response.status_code, 401)

    def test_put_users(self):
        data = {"username": "Test_user",
                "dob": "1991-05-12 09:39:17",
                "password": "password",
                "bio": "Test_user biography"}
        user_id = 5
        response = requests.put(
            'http://localhost:5000/api/users/' + str(user_id), data=data, auth=self.auth)
        self.assertEqual(response.status_code, 401)

    def test_delete_user(self):
        user_id = 7
        response = requests.delete(
            'http://localhost:5000/api/users/' + str(user_id), auth=self.auth)
        self.assertEqual(response.status_code, 401)

    """
    /recipies
    """

    def test_get_recipie(self):
        recipie_id = 1
        response = requests.get(
            'http://localhost:5000/api/recipies/' + str(recipie_id), auth=self.auth)
        self.assertEqual(response.status_code, 401)

    def test_get_recipies(self):
        response = requests.get(
            'http://localhost:5000/api/recipies', auth=self.auth)
        self.assertEqual(response.status_code, 401)

    def test_post_recipies(self):
        data = {"text": "Test recipie text",
                "user_id": 1,
                "title": "Test recipie title"}
        response = requests.post(
            'http://localhost:5000/api/recipies', data=data, auth=self.auth)
        self.assertEqual(response.status_code, 401)

    def test_put_recipies(self):
        data = {"text": "Test recipie text",
                "user_id": 1,
                "title": "Test recipie title"}
        recipie_id = 5
        response = requests.put(
            'http://localhost:5000/api/recipies/' + str(recipie_id), data=data, auth=self.auth)
        self.assertEqual(response.status_code, 401)

    def test_delete_recipie(self):
        recipie_id = 7
        response = requests.delete(
            'http://localhost:5000/api/recipies/' + str(recipie_id), auth=self.auth)
        self.assertEqual(response.status_code, 401)

    """
    /users/{user_id}/recipies
    """
    def test_get_user_recipie(self):
        recipie_id = 1
        user_id=1
        response = requests.get(
            'http://localhost:5000/api/users/' + str(user_id)+'/recipies/'+str(recipie_id), auth=self.auth)
        self.assertEqual(response.status_code, 401)

    def test_get_user_recipies(self):
        user_id=1
        response = requests.get(
            'http://localhost:5000/api/users/' + str(user_id)+'/recipies', auth=self.auth)
        self.assertEqual(response.status_code, 401)

    def test_post_user_recipies(self):
        user_id=1
        data = {"text": "Test recipie text",
                "user_id": user_id,
                "title": "Test recipie title"}
        response = requests.post(
            'http://localhost:5000/api/users/' + str(user_id)+'/recipies', data=data, auth=self.auth)
        self.assertEqual(response.status_code, 401)

    def test_put_user_recipies(self):
        user_id=1
        data = {"text": "Test recipie text",
                "user_id":user_id,
                "title": "Test recipie title"}
        recipie_id = 5
        response = requests.put(
            'http://localhost:5000/api/users/' + str(user_id)+'/recipies/'+str(recipie_id), data=data, auth=self.auth)
        self.assertEqual(response.status_code, 401)

    def test_delete_user_recipie(self):
        user_id=1
        recipie_id = 1
        response = requests.delete(
            'http://localhost:5000/api/users/' + str(user_id)+'/recipies/'+str(recipie_id), auth=self.auth)
        self.assertEqual(response.status_code, 401)

if __name__ == "__main__":
    unittest.main()
