import sqlite3
from flask import Flask,g,request,render_template,redirect
from datetime import datetime
from flask_httpauth import HTTPBasicAuth
# from flask_restful import Api
from recipies import RecipieList,Recipie
from users import User,UserList,UserRecipies,UserRecipiesList
from flask_restful_swagger_2 import Api
from flask_swagger_ui import get_swaggerui_blueprint

DATABASE = 'db.sqlite'
app = Flask("RZNU_REST", static_folder='../static')
api = Api(app, api_version='1.0', api_spec_url='/api/swagger')
auth = HTTPBasicAuth()

SWAGGER_URL = '/api/docs'
API_URL = '/api/swagger.json'  

swaggerui_blueprint = get_swaggerui_blueprint(
    SWAGGER_URL,
    API_URL,
    config={
        'app_name': "RZNU_REST",
        'validatorUrl':None,
        'description':"REST API for the purposes of the lab assignment for SOC course"
    }
)
app.register_blueprint(swaggerui_blueprint, url_prefix=SWAGGER_URL)

def init_db():
    db = get_db()
    with app.open_resource('schema.sql', mode='r') as f:
        db.cursor().executescript(f.read())
    db.commit()

@app.cli.command('initdb')
def initdb_command():
    """Initializes the database."""
    init_db()
    print('Initialized the database.')

def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE)
    return db

@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()
        

@auth.get_password
def get_pw(username):
    db=get_db()
    cur=db.cursor()
    cur.execute("SELECT * FROM users where username=?",[username])
    res=cur.fetchone()
    if res != None:
        return res[2]
    return None

@app.route("/")
def index():
    return redirect('/api/docs')

@app.route("/login",methods=['GET', 'POST'])
def login():
    db=get_db() 
    if request.method=="POST":
        if len(request.form.values())>2:
            username=request.form.get("username")
            password=request.form.get("psw")
            dob=request.form.get("dob")
            bio=request.form.get("bio")
            cur=db.cursor()
            cur.execute("INSERT INTO users (username,password,dob,bio) VALUES (?,?,?,?)", (username,password,dob,bio))
            db.commit()
        else:
            username=request.form.get("username")
            password=request.form.get("psw")
            cur=db.cursor()
            cur.execute("select * from users where username=? and password=?",(username,password))
            if not cur.fetchall():
                print "GTFO"
            else:
                #ovje ide session
                pass
    return render_template('login.html')

@app.route("/user/<id>",methods=['GET', 'POST'])
def user(id):
    db=get_db()
    if request.method=="GET":
        cur=db.cursor()
        cur.execute('select * from users where id=?',id)
        res=cur.fetchone()
        username=res[1]
        dob=res[3]
        bio=res[4]
        return render_template("user.html",username=username,dob=dob,bio=bio)
    elif request.method=="POST":
        cur=db.cursor()
        cur.execute("INSERT INTO users (username,password,dob,bio) VALUES (?,?)", (username,password,dob,bio))


@app.route("/user/<id>/recipies",methods=['GET','POST'])
@auth.login_required
def user_recipies(id):
    db=get_db()
    if request.method=="GET":
        cur=db.cursor()
        cur.execute('select * from recipies where user_id=?',id)
        res=cur.fetchall()
        recipies={}
        for i in res:
            recipies[i[0]]={"text":i[2],"title":i[3],"date":i[4]}
        return render_template("recipies.html",recipies=recipies)

@app.route("/recipie",methods=['GET', 'POST'])
def recipie():
    db=get_db()
    if request.method=="GET":
        cur=db.cursor()
        cur.execute('select * from recipies where id=?',id)
        res=cur.fetchone()
        text=res[2]
        return "placeholder recipie"
    elif request.method=="POST":
        cur=db.cursor()
        #user_id=session.user_id
        user_id=1
        title=request.form.get("title")
        text=request.form.get("recipie")
        created=datetime.now().isoformat(' ')
        cur.execute("INSERT INTO recipies (user_id,text,title,created) VALUES (?,?,?,?)", (user_id,text,title,created))
        db.commit()
    return "asdf"  


api.add_resource(RecipieList,'/api/recipies')
api.add_resource(Recipie,'/api/recipies/<int:recipie_id>',endpoint="recipies")
api.add_resource(UserList,'/api/users')
api.add_resource(User,'/api/users/<int:user_id>',endpoint="users")
api.add_resource(UserRecipiesList,'/api/users/<int:user_id>/recipies')
api.add_resource(UserRecipies,'/api/users/<int:user_id>/recipies/<int:recipie_id>',endpoint="user-recipies")

